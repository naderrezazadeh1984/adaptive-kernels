'''
Describtion: Primary version of Adaptive wavelet
Author: Nader Rezazadeh
Version: V.1.

'''
import CNN
from CNN.network import *
from CNN.utils import *

from tqdm import tqdm
import argparse
import matplotlib.pyplot as plt
import pickle
import pandas as pd

#parser = argparse.ArgumentParser(description='Train a convolutional neural network.')
#parser.add_argument('save_path', metavar = 'Save Path', help='name of file to save parameters in.')

if __name__ == '__main__':
    
   # args = parser.parse_args()
    save_path = "./params2.pkl"
    
    cost = train(save_path = save_path)

    params, cost = pickle.load(open(save_path, 'rb'))
    [f1, f2, w3, w4, b1, b2, b3, b4] = params
    
    # Plot cost 
    plt.plot(cost, 'r')
    plt.xlabel('# Iterations')
    plt.ylabel('Cost')
    plt.legend('Loss', loc='upper right')
    plt.show()

    # Get test data
    m =50
    
    
    
    
    X1=pd.read_csv('Dataset/face_test.csv')
    y_dash1=pd.read_csv('/Dataset/label_test.csv')
    
    X=X1.as_matrix()
    y_dash=y_dash1.as_matrix()
    #y_dash=y_dash.float()
    
    #for i in range(0,399):
     #   y_dash[i]=y_dash1[i,0]
      #  for j in range(0,4095):
       #     X[i,j]=X1[i,j]
            
            
            
            
    
    #X1=np.genfromtxt('/root/faces3.csv',delimiter=',')
    #y_dash1=np.genfromtxt('/root/faces3.csv',delimiter=',')
  
    #print(X1.shape)
   
    #X=np.array(4096,64)
    #y_dash=()
    #X=X1.reshape(m, img_dim)
    #y1_dash=y1_dash.reshape(m,1) 
    
    
    X-= int(np.mean(X))
    #X/= int(np.std(X))
    X=np.true_divide(X,int(np.std(X)))
    train_data = np.hstack((X,y_dash))
    
    test_data = np.hstack((X,y_dash))
    
    X = test_data[:,0:-1]
    X = X.reshape(len(test_data), 1, 64, 64)
    y = test_data[:,-1]

    corr = 0
    digit_count = [0 for i in range(40)]
    digit_correct = [0 for i in range(40)]
   
   
    print("Computing accuracy over test set:")

    t = tqdm(range(len(X)), leave=True)

    for i in t:
        x = X[i]
        pred, prob = predict(x, f1, f2, w3, w4, b1, b2, b3, b4)
        digit_count[int(y[i])]+=1
        if pred==y[i]:
            corr+=1
            digit_correct[pred]+=1

        t.set_description("Acc:%0.2f%%" % (float(corr/(i+1))*100))
        
    print("Overall Accuracy: %.2f" % (float(corr/len(test_data)*100)))
    x = np.arange(40)
    digit_recall = [x/y for x,y in zip(digit_correct, digit_count)]
    plt.xlabel('Digits')
    plt.ylabel('Recall')
    plt.title("Recall on Test Set")
    plt.bar(x,digit_recall)
    plt.show()