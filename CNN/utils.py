'''
Description: intermediate procedure for CNN

Version: V.1.
'''
from CNN.forward import *
import numpy as np
import gzip

#####################################################
################## Utility Methods ##################
#####################################################
        
def extract_data(filename, num_images, IMAGE_WIDTH):
    '''
    Extract images by reading the file bytestream. Reshape the read values into a 3D matrix of dimensions [m, h, w], where m 
    is the number of training examples.
    '''
    print('Extracting', filename)
    with gzip.open(filename) as bytestream:
        bytestream.read(16)
        buf = bytestream.read(IMAGE_WIDTH * IMAGE_WIDTH * num_images)
        data = np.frombuffer(buf, dtype=np.uint8).astype(np.float32)
        data = data.reshape(num_images, IMAGE_WIDTH*IMAGE_WIDTH)
        return data

def extract_labels(filename, num_images):
    '''
    Extract label into vector of integer values of dimensions [m, 1], where m is the number of images.
    '''
    print('Extracting', filename)
    with gzip.open(filename) as bytestream:
        bytestream.read(8)
        buf = bytestream.read(1 * num_images)
        labels = np.frombuffer(buf, dtype=np.uint8).astype(np.int64)
    return labels

def initializeFilter(size, scale = 1.0):
    stddev = scale/np.sqrt(np.prod(size))
    return np.random.normal(loc = 0, scale = stddev, size = size)

def initializeWeight(size):
    return np.random.standard_normal(size=size) * 0.01

def nanargmax(arr):
    idx = np.nanargmax(arr)
    idxs = np.unravel_index(idx, arr.shape)
    return idxs


def predict(image, f1, f2, f3, w4, w5, b1, b2, b3, b4, b5, conv_s=1, pool_f=2, pool_s=2):
    '''
    Make predictions with trained filters/weights.
    '''
    label_=0
    conv1 = convolution(image, f1, b1, conv_s, label_, layer=1)  # convolution operation
    # conv1[conv1<=0] = 0 # pass through ReLU non-linearity
    # pooled1 = maxpool(conv1, pool_f, pool_s)

    conv2 = convolution(conv1, f2, b2, conv_s, label_, layer=2)  # second convolution operation
    # conv2[conv2<=0] = 0 # pass through ReLU non-linearity

    conv3 = convolution(conv2, f3, b3, conv_s, label_, layer=3)  # second convolution operation
    # conv3[conv3<=0] = 0 # pass through ReLU non-linearity

    pooled = maxpool(conv3, pool_f, pool_s)  # maxpooling operation
    #print(pooled.shape)
    (nf3, dim3, _) = pooled.shape
    #print(dim3)
    fc = pooled.reshape((nf3 * dim3 * dim3, 1))  # flatten pooled layer
    #print(nf3)
    z = w4.dot(fc) + b4  # first dense layer
    z[z <= 0] = 0  # pass through ReLU non-linearity

    out = w5.dot(z) + b5  # second dense layer

    probs = softmax(out)  # predict class probabilities with the softmax activation function

    return np.argmax(probs), np.max(probs)
