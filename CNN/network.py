'''
Description: setup and train network 

Author: Naderrezazadeh
Version: V.1.

'''
from CNN.forward import *
from CNN.backward import *
from CNN.utils import *

import numpy as np
import pickle
import math
from tqdm import tqdm
import scipy.io

import h5py
import pandas as pd

import matplotlib.pyplot as plt
import statistics

import os
import cv2
from time import sleep

#####################################################
############### Building The Network ################
#####################################################

def conv(image, label, params, conv_s, pool_f, pool_s):
    
    [f1, f2, f3, w4, w5, b1, b2, b3, b4, b5] = params 
    
    ################################################
    ############## Forward Operation ###############
    ################################################
    conv1 = convolution1(image, f1, b1, conv_s) # convolution operation
    conv1[conv1<=0] = 0 # pass through ReLU non-linearity
  
    conv2 = convolution2(conv1, f2, b2, conv_s) # convolution operation
    conv2[conv2<=0] = 0 # pass through ReLU non-linearity

  
    conv3 = convolution3(conv2, f3, b3, conv_s) # second convolution operation
    conv3[conv3<=0] = 0 # pass through ReLU non-linearity
    
    pooled = maxpool(conv3, pool_f, pool_s) # maxpooling operation
    
    (nf3, dim3, _) = pooled.shape
    fc = pooled.reshape((nf3 * dim3 * dim3, 1)) # flatten pooled layer
    
    z = w4.dot(fc) + b4 # first dense layer
    z[z<=0] = 0 # pass through ReLU non-linearity
    
    out = w5.dot(z) + b5 # second dense layer
     
    probs = softmax(out) # predict class probabilities with the softmax activation function
    
    ################################################
    #################### Loss ######################
    ################################################
    
    loss = categoricalCrossEntropy(probs, label) # categorical cross-entropy loss
        
    ################################################
    ############# Backward Operation ###############
    ################################################
    dout = probs - label # derivative of loss w.r.t. final dense layer output
    dw5 = dout.dot(z.T) # loss gradient of final dense layer weights
    db5 = np.sum(dout, axis = 1).reshape(b5.shape) # loss gradient of final dense layer biases
    
    dz = w5.T.dot(dout) # loss gradient of first dense layer outputs 
    dz[z<=0] = 0 # backpropagate through ReLU 
    dw4 = dz.dot(fc.T)
    db4 = np.sum(dz, axis = 1).reshape(b4.shape)
    
    dfc = w4.T.dot(dz) # loss gradients of fully-connected layer (pooling layer)
    dpool = dfc.reshape(pooled.shape) # reshape fully connected into dimensions of pooling layer
    
    
    dconv3 = maxpoolBackward(dpool, conv3, pool_f, pool_s) # backprop through the max-pooling layer(only neurons with highest activation in window get updated)
    dconv3[conv3<=0] = 0 # backpropagate through ReLU
    
    dconv2, df3, db3 = convolution3Backward(dconv3, conv2, f3, conv_s) # backpropagate previous gradient through second convolutional layer.
    dconv2[conv2<=0] = 0 # backpropagate through ReLU
    
    dconv1, df2, db2 = convolution2Backward(dconv2, conv1, f2, conv_s) # backpropagate previous gradient through second convolutional layer.
    dconv1[conv1<=0] = 0 # backpropagate through ReLU
    
    dimage, df1, db1 = convolution1Backward(dconv1, image, f1, conv_s) # backpropagate previous gradient through first convolutional layer.
    
    grads = [df1, df2, df3, dw4, dw5, db1, db2, db3, db4, db5] 
    
    return grads, loss

#####################################################
################### Optimization ####################
#####################################################

def adamGD(batch, num_classes, lr, dim, n_c, beta1, beta2, params, cost):
    '''
    update the parameters through Adam gradient descnet.
    '''
    [f1, f2, f3, w4, w5, b1, b2, b3, b4, b5] = params
    
    X = batch[:,0:-1] # get batch inputs
    X = X.reshape(len(batch), n_c, dim, dim)
    Y = batch[:,-1] # get batch labels
    
    cost_ = 0
    batch_size = len(batch)
    
    # initialize gradients and momentum,RMS params
    df1 = np.zeros(f1.shape)
    df2 = np.zeros(f2.shape)
    df3 = np.zeros(f3.shape)
    dw4 = np.zeros(w4.shape)
    dw5 = np.zeros(w5.shape)
    db1 = np.zeros(b1.shape)
    db2 = np.zeros(b2.shape)
    db3 = np.zeros(b3.shape)
    db4 = np.zeros(b4.shape)
    db5 = np.zeros(b5.shape)
    
    v1 = np.zeros(f1.shape)
    v2 = np.zeros(f2.shape)
    v3 = np.zeros(f3.shape)
    v4 = np.zeros(w4.shape)
    v5 = np.zeros(w5.shape)
    bv1 = np.zeros(b1.shape)
    bv2 = np.zeros(b2.shape)
    bv3 = np.zeros(b3.shape)
    bv4 = np.zeros(b4.shape)
    bv5 = np.zeros(b5.shape)
    
    s1 = np.zeros(f1.shape)
    s2 = np.zeros(f2.shape)
    s3 = np.zeros(f3.shape)
    s4 = np.zeros(w4.shape)
    s5 = np.zeros(w5.shape)
    bs1 = np.zeros(b1.shape)
    bs2 = np.zeros(b2.shape)
    bs3 = np.zeros(b3.shape)
    bs4 = np.zeros(b4.shape)
    bs5 = np.zeros(b5.shape)
    for i in range(batch_size):
        
        x = X[i]
        y = np.eye(num_classes)[int(Y[i])].reshape(num_classes, 1) # convert label to one-hot
        
        # Collect Gradients for training example
        grads, loss = conv(x, y, params, 1, 2, 2)
        [df1_, df2_, df3_, dw4_, dw5_, db1_, db2_, db3_, db4_, db5_] = grads
        
        df1+=df1_
        db1+=db1_
        df2+=df2_
        db2+=db2_
        df3+=df3_
        db3+=db3_
        dw4+=dw4_
        db4+=db4_
        dw5+=dw5_
        db5+=db5_

        cost_+= loss

    # Parameter Update  
    lr=0.01
    lr1=0.05
    lr2=0.05 
    
    v1 = beta1*v1 + (1-beta1)*df1/batch_size # momentum update
    s1 = beta2*s1 + (1-beta2)*(df1/batch_size)**2 # RMSProp update
    f1 -= lr * v1/np.sqrt(s1+1e-7) # combine momentum and RMSProp to perform update with Adam
          
    bv1 = beta1*bv1 + (1-beta1)*db1/batch_size
    bs1 = beta2*bs1 + (1-beta2)*(db1/batch_size)**2
    b1 -= lr * bv1/np.sqrt(bs1+1e-7)
   
   
   
   
    v2 = beta1*v2 + (1-beta1)*df2/batch_size # momentum update
    s2 = beta2*s2 + (1-beta2)*(df2/batch_size)**2 # RMSProp update
    f2 -= lr * v2/np.sqrt(s2+1e-7) # combine momentum and RMSProp to perform update with Adam
          
    bv2 = beta1*bv2 + (1-beta1)*db2/batch_size
    bs2 = beta2*bs2 + (1-beta2)*(db2/batch_size)**2
    b2 -= lr * bv2/np.sqrt(bs2+1e-7)
   
   
   
   
   
   
    #v2 = beta1*v2 + (1-beta1)*df2/batch_size
    #s2 = beta2*s2 + (1-beta2)*(df2/batch_size)**2
    #f2 -= lr * v2/np.sqrt(s2+1e-7)       
   
   
    v3 = beta1*v3 + (1-beta1)*df3/batch_size
    s3 = beta2*s3 + (1-beta2)*(df3/batch_size)**2
    f3[:,:,:,:,0] -= lr1 * v3[:,:,:,:,0]/np.sqrt(s3[:,:,:,:,0]+1e-7)
    f3[:,:,:,:,1] -= lr2 * v3[:,:,:,:,1]/np.sqrt(s3[:,:,:,:,1]+1e-7)

     
  
         
    bv3 = beta1*bv3 + (1-beta1) * db3/batch_size
    bs3 = beta2*bs3 + (1-beta2)*(db3/batch_size)**2
    b3 -= lr * bv3/np.sqrt(bs3+1e-7)
    
    
    
    
    
    v4 = beta1*v4 + (1-beta1) * dw4/batch_size
    s4 = beta2*s4 + (1-beta2)*(dw4/batch_size)**2
    w4 -= lr * v4/np.sqrt(s4+1e-7)
    
    bv4 = beta1*bv4 + (1-beta1) * db4/batch_size
    bs4 = beta2*bs4 + (1-beta2)*(db4/batch_size)**2
    b4 -= lr * bv4/np.sqrt(bs4+1e-7)
    
    v5 = beta1*v5 + (1-beta1) * dw5/batch_size
    s5 = beta2*s5 + (1-beta2)*(dw5/batch_size)**2
    w5 -= lr * v5 / np.sqrt(s5+1e-7)
    
    bv5 = beta1*bv5 + (1-beta1)*db5/batch_size
    bs5 = beta2*bs5 + (1-beta2)*(db5/batch_size)**2
    b5 -= lr * bv5 / np.sqrt(bs5+1e-7)
    
    
    cost_ = cost_/batch_size
    cost.append(cost_)

    params = [f1, f2, f3, w4, w5, b1, b2, b3, b4, b5]
    
    return params, cost
#####################################################
##################### Training ######################
#####################################################

def train(num_classes = 40, lr = 0.01, beta1 = 0.95, beta2 = 0.99, img_dim = 64, img_depth = 1, f = 5, num_filt1 = 8, num_filt2 = 4, num_filt3 = 4, batch_size = 15, num_epochs = 25, save_path = 'params.pkl'):
    
  
    
    m=500
    
    #batch_size = 
    #X = extract_data('train-images-idx3-ubyte.gz', m, img_dim)
    #print(X.shape)
    #y_dash = extract_labels('train-labels-idx1-ubyte.gz', m).reshape(m,1)
    


    X1=pd.read_csv('Dataset/face_training.csv')
    y_dash1=pd.read_csv('Dataset/label_training.csv')
    X=X1.to_numpy()
    print(X.shape)
    y_dash=y_dash1.to_numpy()
    print(y_dash.shape)



    

     #list files in img directory 

   
    
   
    for idx,val in np.ndenumerate(y_dash):
        print(idx)
        print(val)

    sleep(1)
       
    
   # for i in range(0,399):
    #    y_dash[i,0]=y_dash1[i,0]
     #   for j in range(0,4095):
      #      X[i,j]=X1[i,j]
            
            
            
            
    
    #X1=np.genfromtxt('/root/faces3.csv',delimiter=',')
    #y_dash1=np.genfromtxt('/root/faces3.csv',delimiter=',')
  
    #print(X1.shape)
   
    #X=np.array(4096,64)
    #y_dash=()
    #X=X1.reshape(m, img_dim)
    y_dash=y_dash.reshape((y_dash.shape[0]),1) 
    
    print(X.shape)
    print(y_dash.shape)
    X-= int(np.mean(X))
    #X/= int(np.std(X))
    X=np.true_divide(X,int(np.std(X)))
    print("Noooooooormalized...")
    train_data = np.hstack((X,y_dash))
    print(X)
    sleep(5)
    
    
      
    
    np.random.shuffle(train_data)

    ## Initializing all the parameters
    f1, f2, f3, w4, w5 = (num_filt1 ,img_depth,f,f), (num_filt2 ,num_filt1,f,f), (num_filt3 ,num_filt2,f,f,2), (128,2704), (40, 128)
    f1 = initializeFilter(f1)
    f2 = initializeFilter(f2)
    f3 = initializeFilter(f3)
    w4 = initializeWeight(w4)
    w5 = initializeWeight(w5)

    b1 = np.zeros((f1.shape[0],1))
    b2 = np.zeros((f2.shape[0],1))
    b3 = np.zeros((f3.shape[0],1))
    b4 = np.zeros((w4.shape[0],1))
    b5 = np.zeros((w5.shape[0],1))

    params = [f1, f2, f3, w4, w5, b1, b2, b3, b4, b5]

    cost = []

    print("LR:"+str(lr)+", Batch Size:"+str(batch_size))

    for epoch in range(num_epochs):
        np.random.shuffle(train_data)
        batches = [train_data[k:k + batch_size] for k in range(0, train_data.shape[0], batch_size)]

        t = tqdm(batches)
        for x,batch in enumerate(t):
            params, cost = adamGD(batch, num_classes, lr, img_dim, img_depth, beta1, beta2, params, cost)
            t.set_description("Cost: %.2f" % (cost[-1]))
            
    to_save = [params, cost]
    
    with open(save_path, 'wb') as file:
        pickle.dump(to_save, file)
        
    return cost
        
