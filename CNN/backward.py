'''
Description: CNN Based on Wave Lett
'''

import numpy as np

from CNN.utils import *
import numpy as np

from CNN.utils import *

#####################################################
############### Backward Operations #################
#####################################################
def convolution1Backward(dconv_prev, conv_in, filt, s):
    '''
    Backpropagation through a convolutional layer. 
    '''
    (n_f, n_c, f, _) = filt.shape
    (_, orig_dim, _) = conv_in.shape
    ## initialize derivatives
    dout = np.zeros(conv_in.shape) 
    dfilt = np.zeros(filt.shape)
    dbias = np.zeros((n_f,1))

    for curr_f in range(n_f):
        # loop through all filters
        curr_y = out_y = 0
        while curr_y + f <= orig_dim:
            curr_x = out_x = 0
            while curr_x + f <= orig_dim:
                # loss gradient of filter (used to update the filter)
                dfilt[curr_f,:,:,:] += dconv_prev[curr_f, out_y, out_x] * conv_in[:, curr_y:curr_y+f, curr_x:curr_x+f]
                # loss gradient of the input to the convolution operation (conv1 in the case of this network)
                dout[:, curr_y:curr_y+f, curr_x:curr_x+f] += dconv_prev[curr_f, out_y, out_x] * filt[curr_f,:,:,:] 
                curr_x += s
                out_x += 1
            curr_y += s
            out_y += 1
        # loss gradient of the bias
        dbias[curr_f] = np.sum(dconv_prev[curr_f])
    
    return dout, dfilt, dbias 
 
def convolution2Backward(dconv_prev, conv_in, filt, s):
    '''
    Backpropagation through a convolutional layer. 
    '''
    (n_f, n_c, f, _) = filt.shape
    (_, orig_dim, _) = conv_in.shape
    ## initialize derivatives
    dout = np.zeros(conv_in.shape) 
    dfilt = np.zeros(filt.shape)
    dbias = np.zeros((n_f,1))

    for curr_f in range(n_f):
        # loop through all filters
        curr_y = out_y = 0
        while curr_y + f <= orig_dim:
            curr_x = out_x = 0
            while curr_x + f <= orig_dim:
                # loss gradient of filter (used to update the filter)
                dfilt[curr_f,:,:,:] += dconv_prev[curr_f, out_y, out_x] * conv_in[:, curr_y:curr_y+f, curr_x:curr_x+f]
                # loss gradient of the input to the convolution operation (conv1 in the case of this network)
                dout[:, curr_y:curr_y+f, curr_x:curr_x+f] += dconv_prev[curr_f, out_y, out_x] * filt[curr_f,:,:,:] 
                curr_x += s
                out_x += 1
            curr_y += s
            out_y += 1
        # loss gradient of the bias
        dbias[curr_f] = np.sum(dconv_prev[curr_f])
    
    return dout, dfilt, dbias 






     
def convolution3Backward(dconv_prev, conv_in, filt, s):
    '''
    Backpropagation through a convolutional layer. 
    '''
    (n_f, n_c, f, _, _) = filt.shape
    (_, orig_dim, _) = conv_in.shape
    ## initialize derivatives
    dout = np.zeros(conv_in.shape) 
    dfilt = np.zeros(filt.shape)
    dbias = np.zeros((n_f,1))
    for curr_f in range(n_f):
        # loop through all filters
        curr_y = out_y = 0
        while curr_y + f <= orig_dim:
            curr_x = out_x = 0
            while curr_x + f <= orig_dim:
                # loss gradient of filter (used to update the filter)
                #dfilt[curr_f] += dconv_prev[curr_f, out_y, out_x] * conv_in[:, curr_y:curr_y+f, curr_x:curr_x+f]
                up1_x=(-5/filt[curr_f,:,:,:,1])*np.sin(5*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])/filt[curr_f,:,:,:,1])
                up2_x= np.cos(5*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])/filt[curr_f,:,:,:,1])*(-2*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])/2*(filt[curr_f,:,:,:,1]**2))                
                
                up1_w1=((5/filt[curr_f,:,:,:,1])*np.sin(5*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])/filt[curr_f,:,:,:,1]))
                up2_w1=np.cos(5*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])/filt[curr_f,:,:,:,1])*((conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])/( filt[curr_f,:,:,:,1]**2))
                
                up1_w2=np.sin(5*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])/filt[curr_f,:,:,:,1])*(5*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])/(filt[curr_f,:,:,:,1]**2))
                up2_w2=np.cos(5*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])/filt[curr_f,:,:,:,1])*((conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])**2/(filt[curr_f,:,:,:,1]**3))
                 
                #p1=np.cos(5*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])/filt[curr_f,:,:,:,1])
                #p2=np.exp((-1*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])**2)/(2*filt[curr_f,:,:,:,1]**2))
                
                last=np.exp((-1*(conv_in[:,curr_y:curr_y+f, curr_x:curr_x+f]-filt[curr_f,:,:,:,0])**2)/(2*filt[curr_f,:,:,:,1]**2))
               
                
                up_x=(up1_x+up2_x)*last
                up_w1=(up1_w1+up2_w1)*last
                up_w2=(up1_w2+up2_w2)*last
                
                
                dfilt[curr_f,:,:,:,0] += dconv_prev[curr_f, out_y, out_x] * (up_w1)
                dfilt[curr_f,:,:,:,1] += dconv_prev[curr_f, out_y, out_x] * (up_w2)
                
                
                
                # loss gradient of the input to the convolution operation (conv1 in the case of this network)
                dout[:, curr_y:curr_y+f, curr_x:curr_x+f] += dconv_prev[curr_f, out_y, out_x] * (up_x)  
                curr_x += s
                out_x += 1
            curr_y += s
            out_y += 1
        # loss gradient of the bias
        dbias[curr_f] = np.sum(dconv_prev[curr_f])
    
    return dout, dfilt, dbias



def maxpoolBackward(dpool, orig, f, s):
    '''
    Backpropagation through a maxpooling layer. The gradients are passed through the indices of greatest value in the original maxpooling during the forward step.
    '''
    (n_c, orig_dim, _) = orig.shape
    
    dout = np.zeros(orig.shape)
    
    for curr_c in range(n_c):
        curr_y = out_y = 0
        while curr_y + f <= orig_dim:
            curr_x = out_x = 0
            while curr_x + f <= orig_dim:
                # obtain index of largest value in input for current window
                (a, b) = nanargmax(orig[curr_c, curr_y:curr_y+f, curr_x:curr_x+f])
                dout[curr_c, curr_y+a, curr_x+b] = dpool[curr_c, out_y, out_x]
                
                curr_x += s
                out_x += 1
            curr_y += s
            out_y += 1
        
    return dout
